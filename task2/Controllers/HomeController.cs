﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using task2.Models;

namespace task2.Controllers
{
    public class HomeController : Controller
    {
        CalcService calc;

        public HomeController(CalcService calcService)
        {
            calc = calcService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Add(int firstNum, int secondNum)
        {
            int result = calc.Add(firstNum, secondNum);
            return View("Index", result);
        }

        public IActionResult Sub(int firstNum, int secondNum)
        {
            int result = calc.Sub(firstNum, secondNum);
            return View("Index", result);
        }

        public IActionResult Mul(int firstNum, int secondNum)
        {
            int result = calc.Mul(firstNum, secondNum);
            return View("Index", result);
        }

        public IActionResult Div(int firstNum, int secondNum)
        {
            double result = calc.Div(firstNum, secondNum);
            return View("Index", result);
        }
    }
}