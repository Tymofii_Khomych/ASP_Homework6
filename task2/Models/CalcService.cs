﻿namespace task2.Models
{
    public class CalcService
    {
        public int Add(int a, int b) => a + b;
        public int Sub(int a, int b) => a - b;
        public int Mul(int a, int b) => a * b;
        public double Div(int a, int b) => (double)a / b;
    }
}
