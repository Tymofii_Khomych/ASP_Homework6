﻿namespace task1.Models
{
    public class Days
    {
        public List<string> days { get; set; }

        public Days() 
        {
            days = new List<string>(new string[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" });
        }
    }
}
