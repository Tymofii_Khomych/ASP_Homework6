﻿namespace task1.Models
{
    public class Month
    {
        public List<string> months {get; set;}
        public Days _days;

        public Month(Days days)
        {
            months = new List<string>(new string[] { "January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December" });
            _days = days;   
        }
    }
}
