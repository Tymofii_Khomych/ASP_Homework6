﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Security.Cryptography.X509Certificates;
using task1.Models;

namespace task1.Controllers
{
    public class HomeController : Controller
    {
        Days _days;
        Month _month;

        public HomeController(Days days, Month month)
        {
            _days = days;
            _month = month;
        }

        public IActionResult Index()
        {
            ViewBag.Days = _days;
            ViewBag.Month = _month;

            return View();
        }
    }
}